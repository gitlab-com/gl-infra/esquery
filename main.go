package main

import (
	"github.com/pkg/profile"
	"gitlab.com/gitlab-com/gl-infra/esquery/cmd"
	"os"
)

func main() {
	if pprofMode := os.Getenv("PPROF"); pprofMode != "" {
		switch pprofMode {
		case "cpu":
			defer profile.Start(profile.CPUProfile).Stop()
		case "goroutine":
			defer profile.Start(profile.GoroutineProfile).Stop()
		case "mem":
			defer profile.Start(profile.MemProfile).Stop()
		case "mutex":
			defer profile.Start(profile.MutexProfile).Stop()
		case "block":
			defer profile.Start(profile.BlockProfile).Stop()
		}
	}

	cmd.Execute()
}

