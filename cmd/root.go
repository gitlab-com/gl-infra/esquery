package cmd

import (
	"bufio"
	"bytes"
	"context"
	"github.com/goccy/go-json"
	"github.com/olivere/elastic/v7"
	"github.com/spf13/cobra"
	"io"
	"log"
	"os"
)

// cat query.json | jq '.query' | esquery --index 'pubsub-gcp-events-inf-gprd-*' | jq

// heavily inspired by
//   https://github.com/olivere/elastic/blob/b7de00ddfd128672eac82ef8f6e938bcbb6c2cd4/recipes/sliced_scroll/sliced_scroll.go

var rootCmd = &cobra.Command{
	Use:   "esquery",
	Short: "esquery is a tool for performing efficient bulk queries against elasticsearch.",
	Long: `esquery is a tool for performing efficient bulk queries against elasticsearch.`,
	PersistentPreRun: func(cmd *cobra.Command, args []string) {
		if os.Getenv("ELASTICSEARCH_URL") != "" {
			esURL = os.Getenv("ELASTICSEARCH_URL")
		}
	},
}

var esURL string
var sniff, verbose, debug bool

func Execute() {
	err := rootCmd.Execute()
	if err != nil {
		os.Exit(1)
	}
}

func init() {
	rootCmd.PersistentFlags().StringVar(&esURL, "elasticsearch-url", "http://127.0.0.1:9200", "url of the elasticsearch cluster (can also be provided via ELASTICSEARCH_URL)")
	rootCmd.PersistentFlags().BoolVar(&sniff, "sniff", false, "elasticsearch client-side node discovery")
	rootCmd.PersistentFlags().BoolVar(&verbose, "verbose", false, "log http requests")
	rootCmd.PersistentFlags().BoolVar(&debug, "debug", false, "log http interactions on the wire")
}

type RawAggregation struct {
	raw json.RawMessage
}

func (a RawAggregation) Source() (interface{}, error) {
	return a.raw, nil
}

type customDecoder struct{}

func (u *customDecoder) Decode(data []byte, v interface{}) error {
	return json.Unmarshal(data, v)
}

func outputResults(output string, results *elastic.SearchResult, out chan []byte) error {
	switch output {
	case "source":
		for _, hit := range results.Hits.Hits {
			out <- hit.Source
		}
	case "hit":
		for _, hit := range results.Hits.Hits {
			var jsonBuf []byte
			jsonBuf, err := json.Marshal(hit)
			if err != nil {
				return err
			}
			out <- jsonBuf
		}
	case "agg":
		var jsonBuf []byte
		jsonBuf, err := json.Marshal(results.Aggregations)
		if err != nil {
			return err
		}
		out <- jsonBuf
	case "full":
		var jsonBuf []byte
		jsonBuf, err := json.Marshal(results)
		if err != nil {
			return err
		}
		out <- jsonBuf
	}

	return nil
}

func readFromStdin() (string, error) {
	fi, err := os.Stdin.Stat()
	if err != nil {
		return "", err
	}

	// read from stdin if stdin is a pipe
	if (fi.Mode() & os.ModeCharDevice) == 0 {
		r := bufio.NewReader(os.Stdin)
		buf, err := io.ReadAll(r)
		if err != nil {
			return "", err
		}
		buf = bytes.TrimSpace(buf)
		if len(buf) > 0 {
			return string(buf), nil
		}
	}

	return "", nil
}

func newClient(ctx context.Context) (*elastic.Client, error) {
	var opts []elastic.ClientOptionFunc
	opts = append(opts, elastic.SetURL(esURL))
	opts = append(opts, elastic.SetSniff(sniff))
	opts = append(opts, elastic.SetDecoder(&customDecoder{}))
	opts = append(opts, elastic.SetErrorLog(log.Default()))
	if verbose {
		opts = append(opts, elastic.SetInfoLog(log.Default()))
	}
	if debug {
		opts = append(opts, elastic.SetTraceLog(log.Default()))
	}

	client, err := elastic.NewClient(opts...)
	if err != nil {
		return nil, err
	}

	_, _, err = client.Ping(esURL).Do(ctx)
	if err != nil {
		return nil, err
	}

	return client, nil
}
