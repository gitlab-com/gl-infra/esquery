package cmd

import (
	"context"
	"fmt"
	"github.com/olivere/elastic/v7"
	"github.com/spf13/cobra"
	"log"
	"os"
)

var countIndex string
var countQuery string

var countCmd = &cobra.Command{
	Use:   "count",
	Short: "efficiently retrieve the object count for a query",
	Long: `efficiently retrieve the object count for a query`,
	Run: func(cmd *cobra.Command, args []string) {
		ctx := context.Background()

		if countIndex == "" {
			fmt.Fprintf(os.Stderr, "error: please provide an index pattern via --index\n")
			os.Exit(1)
		}

		if countQuery == "" {
			var err error
			countQuery, err = readFromStdin()
			if err != nil {
				log.Fatal(err)
			}
		}

		client, err := newClient(ctx)
		if err != nil {
			log.Fatal(err)
		}

		var q elastic.Query
		q = elastic.NewMatchAllQuery()
		if countQuery != "" {
			q = elastic.NewRawStringQuery(countQuery)
		}

		count := client.Count().
			Index(countIndex).
			Query(q)

		results, err := count.Do(ctx)
		if err != nil {
			log.Fatal(err)
		}

		fmt.Println(results)
	},
}

func init() {
	rootCmd.AddCommand(countCmd)

	countCmd.PersistentFlags().StringVar(&countIndex, "index", "", "index pattern to query")
	countCmd.PersistentFlags().StringVar(&countQuery, "query", "", "json query to execute (can also be provided via stdin)")
}
