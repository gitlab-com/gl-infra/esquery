package cmd

import (
	"context"
	"fmt"
	"log"
	"os"
	"strconv"

	"github.com/spf13/cobra"
)

var determineShardCountIndex string

var determineShardCountCmd = &cobra.Command{
	Use:   "determine-shard-count",
	Short: "determine and print the most common shard count, for optimal --slices",
	Long: `determine and print the most common shard count, for optimal --slices`,
	Run: func(cmd *cobra.Command, args []string) {
		ctx := context.Background()

		if determineShardCountIndex == "" {
			fmt.Fprintf(os.Stderr, "error: please provide an index pattern via --index\n")
			os.Exit(1)
		}

		client, err := newClient(ctx)
		if err != nil {
			log.Fatal(err)
		}

		// ---

		resp, err := client.IndexGetSettings().
			Index(determineShardCountIndex).
			FlatSettings(true).
			Do(ctx)
		if err != nil {
			log.Fatal(err)
		}

		if len(resp) == 0 {
			log.Fatalf("no index found matching name: %v", determineShardCountIndex)
		}

		dist := make(map[int]int)
		for _, s := range resp {
			shards, err := strconv.Atoi(s.Settings["index.number_of_shards"].(string))
			if err != nil {
				log.Fatal(err)
			}

			if _, ok := dist[shards]; !ok {
				dist[shards] = 0
			}
			dist[shards]++
		}

		maxk := 0
		maxv := 0
		for k, v := range dist {
			if v > maxv {
				maxk = k
				maxv = v
			}
		}

		fmt.Println(maxk)
	},
}

func init() {
	rootCmd.AddCommand(determineShardCountCmd)

	determineShardCountCmd.PersistentFlags().StringVar(&determineShardCountIndex, "index", "", "index pattern to query")
}
