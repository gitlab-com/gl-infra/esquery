package cmd

import (
	"bufio"
	"context"
	"fmt"
	"github.com/olivere/elastic/v7"
	"io"
	"log"
	"os"
	"strings"
	"sync"

	"github.com/spf13/cobra"
)

var scrollIndex string
var scrollQuery string
var scrollOutput string
var scrollSize int
var scrollSourceInclude, scrollSourceExclude, scrollStoredFields, scrollDocvalueFields string
var scrollSlices int
var scrollSliceField string
var scrollOnlySlice int
var scrollKeepAlive string

var scrollCmd = &cobra.Command{
	Use:   "scroll",
	Short: "scan a large number of documents via the scroll API",
	Long: `scan a large number of documents via the scroll API`,
	Run: func(cmd *cobra.Command, args []string) {
		ctx := context.Background()

		if scrollIndex == "" {
			fmt.Fprintf(os.Stderr, "error: please provide an index pattern via --index\n")
			os.Exit(1)
		}

		if scrollOnlySlice != -1 && scrollOnlySlice < 0 || scrollOnlySlice >= scrollSlices {
			fmt.Fprintf(os.Stderr, "error: --only-slice must be in range [0, slices)\n")
			os.Exit(1)
		}

		if scrollSlices < 1 {
			fmt.Fprintf(os.Stderr, "error: --slices must be at least 1\n")
			os.Exit(1)
		}

		if scrollOutput != "source" && scrollOutput != "hit" && scrollOutput != "full" {
			fmt.Fprintf(os.Stderr, "error: --output must be one of: source, hit, full\n")
			os.Exit(1)
		}

		if scrollQuery == "" {
			var err error
			scrollQuery, err = readFromStdin()
			if err != nil {
				log.Fatal(err)
			}
		}

		client, err := newClient(ctx)
		if err != nil {
			log.Fatal(err)
		}

		var q elastic.Query
		q = elastic.NewMatchAllQuery()
		if scrollQuery != "" {
			q = elastic.NewRawStringQuery(scrollQuery)
		}

		out := make(chan []byte, 64)

		var wg sync.WaitGroup

		for i := 0; i < scrollSlices; i++ {
			if scrollOnlySlice != -1 && scrollOnlySlice != i {
				continue
			}

			wg.Add(1)
			go func(i int) {
				defer wg.Done()

				sliceQuery := elastic.NewSliceQuery().Id(i).Max(scrollSlices)
				if scrollSliceField != "" {
					sliceQuery.Field(scrollSliceField)
				}

				fetchSourceContext := elastic.NewFetchSourceContext(true)
				if scrollSourceInclude != "" {
					include := strings.Split(scrollSourceInclude, ",")
					fetchSourceContext.Include(include...)
				}
				if scrollSourceExclude != "" {
					exclude := strings.Split(scrollSourceExclude, ",")
					fetchSourceContext.Exclude(exclude...)
				}

				ss := elastic.NewSearchSource().
					FetchSourceContext(fetchSourceContext)
				if scrollSlices > 1 {
					ss.Slice(sliceQuery)
				}
				if scrollStoredFields != "" {
					stored := strings.Split(scrollStoredFields, ",")
					ss.StoredFields(stored...)
				}
				if scrollDocvalueFields != "" {
					docvalue := strings.Split(scrollDocvalueFields, ",")
					ss.DocvalueFields(docvalue...)
				}

				scroll := client.Scroll().
					SearchSource(ss).
					Index(scrollIndex).
					Query(q).
					Size(scrollSize).
					Scroll(scrollKeepAlive)

				for {
					results, err := scroll.Do(ctx)

					if err == io.EOF {
						break
					}
					if err != nil {
						log.Fatal(err)
					}

					err = outputResults(scrollOutput, results, out)
					if err != nil {
						log.Fatal(err)
					}
				}

				err = scroll.Clear(ctx)
				if err != nil {
					log.Fatal(err)
				}
			}(i)
		}

		go func() {
			wg.Wait()
			close(out)
		}()

		w := bufio.NewWriter(os.Stdout)
		defer w.Flush()

		for b := range out {
			w.Write(b)
			w.Write([]byte("\n"))
		}
	},
}

func init() {
	rootCmd.AddCommand(scrollCmd)

	scrollCmd.PersistentFlags().StringVar(&scrollIndex, "index", "", "index pattern to query")
	scrollCmd.PersistentFlags().StringVar(&scrollQuery, "query", "", "json query to execute (can also be provided via stdin)")
	scrollCmd.PersistentFlags().StringVar(&scrollOutput, "output", "source", "scope how much of the result to print, must be one of: source, hit, full")
	scrollCmd.PersistentFlags().IntVar(&scrollSize, "size", 1000, "batch size for each scroll operation")
	scrollCmd.PersistentFlags().StringVar(&scrollSourceInclude, "source-include", "", "fields to include in the response")
	scrollCmd.PersistentFlags().StringVar(&scrollSourceExclude, "source-exclude", "", "fields to exclude from the response")
	scrollCmd.PersistentFlags().StringVar(&scrollStoredFields, "stored-fields", "", "stored fields to include")
	scrollCmd.PersistentFlags().StringVar(&scrollDocvalueFields, "docvalue-fields", "", "docvalue fields to include")
	scrollCmd.PersistentFlags().IntVar(&scrollSlices, "slices", 4, "number of slices to partition the scroll by")
	scrollCmd.PersistentFlags().StringVar(&scrollSliceField, "slice-field", "", "field to slice by")
	scrollCmd.PersistentFlags().IntVar(&scrollOnlySlice, "only-slice", -1, "limit to a single slice, useful for partitioning or parallel processing of the output, zero-indexed")
	scrollCmd.PersistentFlags().StringVar(&scrollKeepAlive, "keep-alive", "1m", "keep-alive duration for scroll operation")
}
