package cmd

import (
	"bufio"
	"context"
	"fmt"
	"github.com/goccy/go-json"
	"github.com/olivere/elastic/v7"
	"log"
	"os"
	"strings"

	"github.com/spf13/cobra"
)

var searchIndex string
var searchQuery string
var searchOutput string
var searchSize int
var searchAggs string
var searchSourceInclude, searchSourceExclude, searchStoredFields, searchDocvalueFields string

var searchCmd = &cobra.Command{
	Use:   "search",
	Short: "perform aggregations via the search API",
	Long: `perform aggregations via the search API`,
	Run: func(cmd *cobra.Command, args []string) {
		ctx := context.Background()

		if searchIndex == "" {
			fmt.Fprintf(os.Stderr, "error: please provide an index pattern via --index\n")
			os.Exit(1)
		}

		if searchOutput != "source" && searchOutput != "hit" && searchOutput != "agg" && searchOutput != "full" {
			fmt.Fprintf(os.Stderr, "error: --output must be one of: source, hit, agg, full\n")
			os.Exit(1)
		}

		if searchQuery == "" {
			var err error
			searchQuery, err = readFromStdin()
			if err != nil {
				log.Fatal(err)
			}
		}

		client, err := newClient(ctx)
		if err != nil {
			log.Fatal(err)
		}

		var q elastic.Query
		q = elastic.NewMatchAllQuery()
		if searchQuery != "" {
			q = elastic.NewRawStringQuery(searchQuery)
		}

		aggsMap := make(map[string]json.RawMessage)
		if searchAggs != "" {
			err = json.Unmarshal([]byte(searchAggs), &aggsMap)
			if err != nil {
				log.Fatal(err)
			}
		}

		out := make(chan []byte, 64)

		fetchSourceContext := elastic.NewFetchSourceContext(true)
		if searchSourceInclude != "" {
			include := strings.Split(searchSourceInclude, ",")
			fetchSourceContext.Include(include...)
		}
		if searchSourceExclude != "" {
			exclude := strings.Split(searchSourceExclude, ",")
			fetchSourceContext.Exclude(exclude...)
		}

		ss := elastic.NewSearchSource().
			FetchSourceContext(fetchSourceContext)
		if searchStoredFields != "" {
			stored := strings.Split(searchStoredFields, ",")
			ss.StoredFields(stored...)
		}
		if searchDocvalueFields != "" {
			docvalue := strings.Split(searchDocvalueFields, ",")
			ss.DocvalueFields(docvalue...)
		}

		for name, agg := range aggsMap {
			ss.Aggregation(name, RawAggregation{agg})
		}

		search := client.Search().
			SearchSource(ss).
			Index(searchIndex).
			Query(q).
			Size(searchSize)

		go func() {
			results, err := search.Do(ctx)
			if err != nil {
				log.Fatal(err)
			}

			err = outputResults(searchOutput, results, out)
			if err != nil {
				log.Fatal(err)
			}

			close(out)
		}()

		w := bufio.NewWriter(os.Stdout)
		defer w.Flush()

		for b := range out {
			w.Write(b)
			w.Write([]byte("\n"))
		}
	},
}

func init() {
	rootCmd.AddCommand(searchCmd)

	searchCmd.PersistentFlags().StringVar(&searchIndex, "index", "", "index pattern to query")
	searchCmd.PersistentFlags().StringVar(&searchQuery, "query", "", "json query to execute (can also be provided via stdin)")
	searchCmd.PersistentFlags().StringVar(&searchOutput, "output", "agg", "scope how much of the result to print, must be one of: source, hit, full")
	searchCmd.PersistentFlags().IntVar(&searchSize, "size", 0, "batch size for each search operation")
	searchCmd.PersistentFlags().StringVar(&searchAggs, "aggs", "", "json aggregations")
	searchCmd.PersistentFlags().StringVar(&searchSourceInclude, "source-include", "", "fields to include in the response")
	searchCmd.PersistentFlags().StringVar(&searchSourceExclude, "source-exclude", "", "fields to exclude from the response")
	searchCmd.PersistentFlags().StringVar(&searchStoredFields, "stored-fields", "", "stored fields to include")
	searchCmd.PersistentFlags().StringVar(&searchDocvalueFields, "docvalue-fields", "", "docvalue fields to include")
}
