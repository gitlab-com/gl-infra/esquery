# esquery

`esquery` is a tool for performing efficient bulk queries against elasticsearch.

## Install

```bash
go install gitlab.com/gitlab-com/gl-infra/esquery@latest
```

## Usage

### `scroll`

The main use case for esquery is the `scroll` subcommand which allows querying an index pattern directly.

Here is an example that exports all `gcp-events` in a certain time range.

```bash
export ELASTICSEARCH_URL=...
esquery scroll --index 'pubsub-gcp-events-inf-gprd-*' --query '{"range":{"json.receiveTimestamp":{"gte": "2021-12-06T17:19:07.068Z","lte": "2021-12-06T20:19:07.068Z"}}}'
```

The query can also be provided via stdin. This is useful when exporting the query from Kibana's "Inspect" tab.

This example reads the query from stdin and processes the results.

```bash
cat feature_flag_states_exists.json | jq .query | esquery scroll --index 'pubsub-rails-inf-gprd-*' | awk '{ print length($0) }' | datamash count 1 mean 1 median 1 min 1 max 1
```

#### Slices

Queries are executed via a sliced scroll. This allows processing the query in parallel.

The default number of slices is `2`, but this can be overridden via `-slices`.

```bash
cat feature_flag_states_exists.json | jq .query | esquery scroll --index 'pubsub-rails-inf-gprd-*' --slices 9 | wc -l
```

Ideally the number of slices should match the number of shards. To determine the most frequent shard count of an index, you can use the `determine-shard-count` subcommand.

```bash
esquery determine-shard-count --index 'pubsub-rails-inf-gprd-*'
```

It is also possible to limit the execution to only a single slice. This allows running multiple `esquery` processes in parallel. This can be useful for parallelizing post-processing.

```bash
seq 0 8 | parallel 'cat feature_flag_states_exists.json | jq .query | esquery scroll --index pubsub-rails-inf-gprd-\* --slices 9 --only-slice {} | wc -l' | datamash sum 1
```

#### Output

It is possible to define the output scope via `--output`, which can be set to one of: `source` (default), `hit`, `full`.

- By default, `esquery` will output one `_source` document per line, as json.
- When set to `hit`, it will include the hit-level metadata (one level above `_source`).
- When set to `full`, it will incldue the full json from Elasticsearch.

### `search`

In order to use the search API, for example for aggregations, you can use the `search` subcommand.

Here is an example of several aggregations being performed at the same time:

```bash
esquery search --index 'pubsub-gitaly-inf-gprd-*' --query '{"range":{"json.time":{"gte":"now-1d","lte":"now"}}}' --aggs '{"docs_count":{"value_count":{"field":"@timestamp"}},"min_timestamp":{"min":{"field":"@timestamp"}},"max_timestamp":{"max":{"field":"@timestamp"}}}'
```

Search supports the same `--output` flag as `scroll` but with an additional `agg` value, which is the default.

### `count`

The `count` subcommand enables efficient document counting according to a query.

Example:

```bash
esquery count --index 'pubsub-gitaly-inf-gprd-*' --query '{"range":{"json.time":{"gte":"now-1d","lte":"now"}}}'
```

## Verbosity

To understand what `esquery` is doing, you can enable verbose mode via `-verbose`. This will log requests made to `stderr`.

For debugging purposes, you can dump all HTTP interactions via `-debug`.
