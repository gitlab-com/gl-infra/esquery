module gitlab.com/gitlab-com/gl-infra/esquery

go 1.17

require (
	github.com/goccy/go-json v0.9.7
	github.com/olivere/elastic/v7 v7.0.32
	github.com/pkg/profile v1.6.0
	github.com/spf13/cobra v1.4.0
)

require (
	github.com/inconshreveable/mousetrap v1.0.0 // indirect
	github.com/josharian/intern v1.0.0 // indirect
	github.com/mailru/easyjson v0.7.7 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/spf13/pflag v1.0.5 // indirect
)
